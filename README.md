# web-gimi-viewer

A proof-of-concept viewer for ISOBMFF+HEIF files containing geographical data (according to the GIMI profile), e.g. satellite imagery.

This can be tested online at:
- https://ivansanchez.gitlab.io/web-gimi-viewer/index.html (text output)
- https://ivansanchez.gitlab.io/web-gimi-viewer/map.html (visual map output)

Note that any image files used in that demo are **not** uploaded anywhere; they are processed locally in your browser.


This viewer uses custom-built versions of:
- mp4box https://github.com/gpac/mp4box.js/ (BSD-3 license)
- vidterra's misb.js https://github.com/vidterra/misb.js/ (MIT license)

...as well as an unmodified copy of:
- heic2any https://github.com/alexcorvi/heic2any (MIT license)

...and dynamically loads from a CDN:
- gleo https://gitlab.com/IvanSanchez/gleo (GPL)
